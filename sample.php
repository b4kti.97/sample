@extends('operator.template')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Layanan E-KTP
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="{{url('layanan')}}">layanan</a></li>
      <li class="active">Layanan E-KTP</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="box box-warning collapsed-box">
      <div class="box-header">
        <i class="fa fa-lightbulb-o"></i>
        <h3 class="box-title"><strong>Informasi ...</strong></h3>
        <!-- tools box -->
        <div class="pull-right box-tools">
          <button class="btn btn-warning btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
          <button class="btn btn-warning btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div><!-- /. tools -->
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="callout callout-warning">
          <p>Halaman ini digunakan untuk menampilkan detail permintaan layanan yang dilakukan penduduk. jika ingin di update menjadi konfirmasi klik tombol <button type="button" class="btn btn-sm btn-success" name="button"><span class="fa fa-check"></span> Konfirmasi</button>
          atau Klik tombol <button type="button" class="btn btn-sm btn-danger" name="button"><span class="fa fa-arrow-left"></span> Cancel</button> jika ingin membatalkan.</p>
        </div>
      </div>
    </div>
    {!! Form::open(['url'=>'layanan/konfirmasi_proses', 'files'=>true, 'class'=>'form', 'id'=>'form']) !!}
    @foreach($data as $tampil)
      <div class="box box-primary">
        <div class="box-header with-border">
					<h3 class="box-title"><span class="fa fa-user"></span> Detail Layanan E-KTP</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <h5>No E-KTP :</h5>
              <div class="form-group">
                <div class=" input-group">
                  <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                  <input class="form-control" value="{{$tampil->no_kk}}" disabled=""/>
                </div>
              </div>
              <div class="col-md-5">
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="col-md-6">
              <h5>NIK Pembuat :</h5>
              <div class="form-group">
                <div class=" input-group">
                  <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                  <input class="form-control" value="{{$tampil->nik_pembuat}}" disabled=""/>
                </div>
              </div>
              <h5>Berkas Surat Pengantar :   <button type="button" class="btn btn-primary" name="button" onclick="detail_bk_pengantar('{{$tampil->bk_surat_pengantar}}')" title="Lihat"><span class="fa fa-eye"></span></button> </h5>
              <div class="form-group">
                <div class="uploadimage">
                  <img id="uploadPreviewsp" class="thumb-image" src="{{asset('assets/uploads/data_ektp/surat_pengantar/'.$tampil->bk_surat_pengantar)}}" style="max-height: 300px;"/>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <h5>Nama Pembuat :</h5>
              <div class="form-group">
                <div class=" input-group">
                  <span class="input-group-addon" id="basic-addon1"><span class="fa fa-user"></span></span>
                  <input class="form-control" value="{{$tampil->nama_lengkap}}" disabled=""/>
                </div>
              </div>
              <h5>Berkas Kartu Keluarga :  <button type="button" class="btn btn-primary" name="button" onclick="detail_bk_kk('{{$tampil->bk_kk}}')" title="Lihat"><span class="fa fa-eye"></span></button> </h5>
              <div class="form-group">
                <div class="uploadimage">
                  <img id="uploadPreviewsp" class="thumb-image" src="{{asset('assets/uploads/data_ektp/kk/'.$tampil->bk_kk)}}" style="max-height: 300px;"/>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <h5>Berkas F1.21 :  <button type="button" class="btn btn-primary" name="button" onclick="detail_bk_f1_21('{{$tampil->bk_f1_21}}')" title="Lihat"><span class="fa fa-eye"></span></button> </h5>
              <div class="form-group">
                <div class="uploadimage">
                  <img id="uploadPreviewsp" class="thumb-image" src="{{asset('assets/uploads/data_ektp/f1_21/'.$tampil->bk_f1_21)}}" style="max-height: 300px;"/>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <h5>Berkas Surat Kehilangan :  <button type="button" class="btn btn-primary" name="button" onclick="detail_bk_surat_kehilangan('{{$tampil->bk_surat_kehilangan}}')" title="Lihat"><span class="fa fa-eye"></span></button> </h5>
              <div class="form-group">
                <div class="uploadimage">
                  <img id="uploadPreviewsp" class="thumb-image" src="{{asset('assets/uploads/data_ektp/surat_kehilangan/'.$tampil->bk_surat_kehilangan)}}" style="max-height: 300px;"/>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <h5>Alasan :</h5>
              <div class="form-group">
                <div class=" input-group">
                  <span class="input-group-addon" id="basic-addon1"><span class="fa fa-edit"></span></span>
                  <textarea class="form-control" value="{{$tampil->nama_lengkap}}" disabled=""/>{{$tampil->alasan}}</textarea>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <div class="pull-right">
            <input type="hidden" name="id" value="{{$tampil->id_ektp}}">
            <input type="hidden" name="nik" value="{{$tampil->nik_pembuat}}"/>
            <input type="text" name="id_laplay" value="{{$id_laplay}}"/>
            <button type="button" class="btn btn-info" onclick="kirimpesan()">Kirim Pesan <i class="fa fa-envelope-o"></i></button>
            <a href="{{url('layanan')}}" type="button" class="btn btn-danger" name="button"><span class="fa fa-arrow-left"></span> Batal</a>
            <button type="button" class="btn btn-success" id="setValueButton">Konfirmasi <i class="fa fa-check"></i></button>
          </div>
        </div><!-- /.box-footer -->
      </div><!-- /. box -->
      @endforeach
    {!! Form::close() !!}
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@include('operator.layanan_penduduk.data_ektp.popup_berkas')
@include('operator.layanan_penduduk.data_ektp.popup_kirimpesan')
@stop
@section('tambahan_js')
<!-- <script src="http://malsup.github.com/jquery.form.js"></script> -->
<script type="text/javascript" src="{{asset('assets/styles/tambahan/show_password/show_password.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{asset('assets/styles/tambahan/js/jquery.form.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{asset('assets/styles/tambahan/js/formValidation.min.js')}}"></script>
<script language="javascript" type="text/javascript" src="{{asset('assets/styles/tambahan/js/bootstrap_val.min.js')}}"></script>
<script>
$(document).ready(function(){
    isvalid = false;
    $('#formKirimPesan').formValidation({
      framework: 'bootstrap',
      excluded: 'disabled',

      icon: {
        valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        pesan: {
          validators: {
            notEmpty: {
              message: 'Pesan Tidak Boleh Kosong'
            }
          }
        }
      }
    }).on('err.field.fv', function(e, data) {
         isvalid = false;
    }).on('success.form.fv', function(e, data) {
         isvalid = true;
    });

     $('#setValueButton').on('click', function() {
      submitForm();
    });

    $('#kirimpesan').on('click', function() {
     sub_kirimpesan();
   });

    function submitForm(){
      swal({
          title: "Konfirmasi",
          text: "Yakin dengan data yang akan dikonfirmasi ?",
          type: "info",
          background : "rgba(1, 52, 93, 0.38)",
          showCancelButton: true,
          confirmButtonColor: "#00a65a",
          confirmButtonText: "Ya, Konfirmasi",
          cancelButtonText: "Tidak, Batal!",
          closeOnConfirm: true,
          closeOnCancel: true,
          animation: false,
          customClass: "animated zoomInUp"
        }).then(function () {
          document.getElementById('form').submit();
        });
    }

    function sub_kirimpesan(){
      $('#formKirimPesan').data('formValidation').validate();
      if(isvalid){
        swal({
          title: "Konfirmasi",
          text: "Yakin akan mengirim pesan ?",
          type: "info",
          background : "rgba(1, 52, 93, 0.38)",
          showCancelButton: true,
          confirmButtonColor: "#00a65a",
          confirmButtonText: "Ya, Konfirmasi",
          cancelButtonText: "Tidak, Batal!",
          closeOnConfirm: true,
          closeOnCancel: true,
          animation: false,
          customClass: "animated zoomInUp"
        }).then(function () {
          document.getElementById('formKirimPesan').submit();
        });
      }
    }

    $('#form').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) {
        e.preventDefault();
        if(!isvalid){
          submitForm();
        }else {
          return false;
        }
      }
    });
  });

    $(function() {
  			//Datemask dd/mm/yyyy
  			$("#datemask").inputmask("dd/mm/yyyy", {
  					"placeholder": "dd/mm/yyyy"
  			});
  			//Money Euro
  			$("[data-mask]").inputmask();
  	});

  $(function () {
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
  });

  function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9\s\-()+\.]|\./;
    if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  function validate_kata(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[a-zA-Z, ]|\./;
    if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  function detail_bk_pengantar(bk_surat_pengantar)
  {
    document.getElementById("previewBerkas").src = "{{asset('assets/uploads/data_ektp/surat_pengantar/')}}"+"/"+bk_surat_pengantar;
    $('.berkas').modal('show');
  }

  function detail_bk_f1_21(bk_f1_21)
  {
    document.getElementById("previewBerkas").src = "{{asset('assets/uploads/data_ektp/f1_21/')}}"+"/"+bk_f1_21;
    $('.berkas').modal('show');
  }

  function detail_bk_surat_kehilangan(bk_surat_kehilangan)
  {
    document.getElementById("previewBerkas").src = "{{asset('assets/uploads/data_ektp/surat_kehilangan/')}}"+"/"+bk_surat_kehilangan;
    $('.berkas').modal('show');
  }

  function detail_bk_kk(bk_kk)
  {
    document.getElementById("previewBerkas").src = "{{asset('assets/uploads/data_ektp/kk/')}}"+"/"+bk_kk;
    $('.berkas').modal('show');
  }

  function kirimpesan(){
    $(".kirimpesan").modal('show');
  }
</script>
<script type="text/javascript">
function PreviewImagesp() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadImagesp").files[0]);
  oFReader.onload = function (oFRpenduduk)
  {
    document.getElementById("uploadPreviewsp").src = oFRpenduduk.target.result;
  };
};
</script>
@stop
